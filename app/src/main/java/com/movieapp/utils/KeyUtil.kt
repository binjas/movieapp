package com.movieapp.utils


object KeyUtil {
    // api params start
    const val KEYWORD = "keyword"
    const val OFFSET = "offset"
    // api params end
    const val SEARCH_KEYWORD = "searchKeyWord"
    const val MOVIE_STATUS = "movieStatus"
    const val MOVIE_STATUS_NOW_SHOWING = 0
    const val MOVIE_STATUS_UPCOMING = 1

    const val MOVIE_OBJECT = "MOVIE_OBJECT"
    const val AUTO_ANIMATE_TIME = 5000L
    const val ALPHA_ANIMATION_TIME = 300L

    const val FADE_FACTOR = 0.75F

    const val FULL_BIAS = 1F
    const val HALF_BIAS = 0.5F

    const val MARGIN_DEFAUL_VALUE = 40

    const val DATE_FORMATE = "dd MMM yyyy"
}