package com.movieapp.utils

import android.content.Context
import android.widget.Toast


object ToastUtils {
    fun toast(context: Context?, message: String?) {
        if (context != null && message != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun toast(context: Context, message: Int) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}


