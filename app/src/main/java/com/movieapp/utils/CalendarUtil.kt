package com.movieapp.utils

import android.text.format.DateFormat
import com.movieapp.utils.KeyUtil.DATE_FORMATE
import java.util.*

object CalendarUtil {

    fun getDateFromTimestamp(timestamp: Long): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = timestamp*1000
        return DateFormat.format(DATE_FORMATE, cal.time).toString()
    }
}