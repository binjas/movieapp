package com.movieapp.extension

import android.annotation.SuppressLint
import android.content.res.Resources
import android.util.TypedValue
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.movieapp.R

/**
 * To load an image with round corner
 */
fun ImageView.getRoundCornerOption(): RequestOptions {
    return RequestOptions()
        .placeholder(R.drawable.placeholder)
        .transforms(CenterCrop(), RoundedCorners(dp2px(this.resources, 16)))
}

/**
 * to loadImage network image
 * source: network image url
 * mImageView: instance of ImageView
 * requestOptions: Configured request option as per the requirement.
 */
@SuppressLint("CheckResult")
fun ImageView.loadImage(source: String?, requestOptions: RequestOptions? = null) {
    var requestOption: RequestOptions? = null
    val requestBuilder = Glide.with(context)
        .load(source ?: "")
    requestOption = requestOptions ?: RequestOptions()
    requestOption.placeholder(R.drawable.placeholder)
    requestOption.diskCacheStrategy(DiskCacheStrategy.ALL)
    requestBuilder.apply(requestOption)
    requestBuilder.into(this)
}

/**
 * Convert dp to px
 */
fun dp2px(resource: Resources, dp: Int): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resource.displayMetrics).toInt()
}
