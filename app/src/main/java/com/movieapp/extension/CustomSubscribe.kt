package com.movieapp.extension

import com.movieapp.data.common.exceptions.MovieAppHttpException
import com.movieapp.domain.common.CustomErrorConsumer
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

fun <T> Single<T>.customSubscribe(success: (T) -> Unit, error: (MovieAppHttpException) -> Unit): Disposable {
    return this.subscribe(Consumer {
        success(it)
    }, object : CustomErrorConsumer() {
        override fun accept(it: MovieAppHttpException) {
            error(it)
        }
    })
}