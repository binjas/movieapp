package com.movieapp.extension

fun String.isRequiredField(): Boolean {
    return isNotEmpty() && isNotBlank()
}
