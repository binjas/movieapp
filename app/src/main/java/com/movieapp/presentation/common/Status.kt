package com.movieapp.presentation.common

enum class Status {
    SUCCESS,
    LOADING,
    ERROR,
    NO_INTERNET,
    EMPTY_PHONE_NUMBER,
    IN_VALID_PHONE_NUMBER,
    EMPTY_OTP,
    INVALID_OTP,
    ACCOUNT_NOT_BOUNDED_WITH_DANA,
    LOGOUT_SUCCESS
}