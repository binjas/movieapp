package com.movieapp.presentation.common

class Resource<T>(val status: Status, val throwable: Throwable)