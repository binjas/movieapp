package com.movieapp.presentation.common.module

import com.movieapp.presentation.history.HistoryViewModelFactory
import com.movieapp.presentation.movie.home.HomeViewModelFactory
import com.movieapp.presentation.movie.movieList.tab.MovieListViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelFactoryModule::class])
interface ViewModelFactoryComponent {
    fun inject(target: HomeViewModelFactory)
    fun inject(target: MovieListViewModelFactory)
    fun inject(target: HistoryViewModelFactory)
}