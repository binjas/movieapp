package com.movieapp.presentation.common

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.movieapp.R
import com.movieapp.utils.KeyUtil.MARGIN_DEFAUL_VALUE

class TransViewPager @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    ViewPager(context, attrs), ViewPager.PageTransformer {

    private var maxScale = 0.0f // default scale
    private var viewPageMargin = 0 // default margin
    private var animationEnabled = true
    private var fadeEnabled = false
    private var fadeFactor = 0.5f // default fade

    init {
        // clipping should be off on the pager for its children so that they can scale out of bounds.
        clipChildren = false
        clipToPadding = false
        overScrollMode = 2 // set scroll mode 2
        setPageTransformer(false, this)
        offscreenPageLimit = 3 // set screen offset
        viewPageMargin = dp2px(context.resources, MARGIN_DEFAUL_VALUE)
        setPadding(viewPageMargin, viewPageMargin, viewPageMargin, viewPageMargin)
    }

    /**
     * Convert dp to px
     *
     * @param: resource: Resources
     * @param: dp: dp values
     */
    private fun dp2px(resource: Resources, dp: Int): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resource.displayMetrics).toInt()
    }

    /**
     * Set animation for the viewpager
     *
     * @param: enable: true if need to animate
     */
    fun setAnimationEnabled(enable: Boolean) {
        this.animationEnabled = enable
    }

    /**
     * Set fade animation for the child view
     *
     * @param: fadeEnabled: true if need to animate
     */
    fun setFadeEnabled(fadeEnabled: Boolean) {
        this.fadeEnabled = fadeEnabled
    }

    /**
     * Set fade values for the child view
     *
     * @param: fadeFactor: fade value
     */
    fun setFadeFactor(fadeFactor: Float) {
        this.fadeFactor = fadeFactor
    }

    override fun setPageMargin(marginPixels: Int) {
        viewPageMargin = marginPixels
        setPadding(viewPageMargin, viewPageMargin, viewPageMargin, viewPageMargin)
    }

    override fun transformPage(page: View, tranPosition: Float) {

        // Get the bottom view from child.
        val tvBuy = page.findViewById<TextView>(R.id.tvBuy)
        tvBuy.alpha = 0f
        tvBuy.pivotY = tvBuy.height.toFloat()

        var position = tranPosition
        if (viewPageMargin <= 0 || !animationEnabled)
            return
        page.setPadding(viewPageMargin / 3, viewPageMargin / 3, viewPageMargin / 3, viewPageMargin / 3)

        if (maxScale == 0.0f && position > 0.0f && position < 1.0f) {
            maxScale = position
        }
        position -= maxScale
        val absolutePosition = Math.abs(position)
        if (position <= -1.0f || position >= 1.0f) {
            // Page is not visible -- stop any running animations
            if (fadeEnabled) {
                page.alpha = fadeFactor
            }
        } else if (position == 0.0f) {
            // Page is selected -- reset any views if necessary
            page.scaleX = 1 + maxScale
            page.scaleY = 1 + maxScale
            page.alpha = 1f
            // Updated alpha and scale animation to buy ticket button
            tvBuy.alpha = 1f
            tvBuy.scaleY = 1f
        } else {
            page.scaleX = 1 + maxScale * (1 - absolutePosition)
            page.scaleY = 1 + maxScale * (1 - absolutePosition)
            if (fadeEnabled) {
                page.alpha = Math.max(fadeFactor, 1 - absolutePosition)
                // Updated alpha and scale animation to buy ticket button
                tvBuy.alpha = Math.max(0f, 1 - absolutePosition)
                tvBuy.scaleY = Math.max(0f, 1 - absolutePosition)
            }
        }
    }

}