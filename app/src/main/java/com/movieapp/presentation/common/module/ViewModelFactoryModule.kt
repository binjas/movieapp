package com.movieapp.presentation.common.module

import com.movieapp.data.history.HistoryModule
import com.movieapp.data.movie.MovieModule
import dagger.Module

@Module(includes = [MovieModule::class, HistoryModule::class])
class ViewModelFactoryModule
