package com.movieapp.presentation.baseutil

import android.os.Bundle
import androidx.annotation.CallSuper

abstract class BaseViewModelFragment<T : BaseViewModel> : BaseFragment() {

    protected val viewModel by lazy { buildViewModel() }

    protected abstract fun buildViewModel(): T

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        initLiveDataObservers()
        super.onActivityCreated(savedInstanceState)
    }

    /**
     * in this method, we will set observer for view model
     */
    @CallSuper
    protected open fun initLiveDataObservers() {
    }

}