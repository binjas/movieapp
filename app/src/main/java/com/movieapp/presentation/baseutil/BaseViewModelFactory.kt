package com.movieapp.presentation.baseutil

import androidx.lifecycle.ViewModelProvider
import com.movieapp.presentation.common.module.DaggerViewModelFactoryComponent
import com.movieapp.presentation.common.module.ViewModelFactoryComponent

abstract class BaseViewModelFactory: ViewModelProvider.Factory {

    protected val component: ViewModelFactoryComponent by lazy {
        DaggerViewModelFactoryComponent.builder().build()
    }
}