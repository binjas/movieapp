package com.movieapp.presentation.baseutil

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    /**
     * this method will be used to get id of inflating layout.
     */
    protected abstract fun getInflatingLayout(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getInflatingLayout(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeViews()
    }

    /**
     * this method will be called to initialize views, it's visibility,
     * text update, state update and many other in the same way
     */
    protected open fun initializeViews() {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        handleBusinessLogic()
    }

    /**
     * this method will be used to make call with database, webservice using retrofit, etc.
     */
    protected open fun handleBusinessLogic() {
    }
}