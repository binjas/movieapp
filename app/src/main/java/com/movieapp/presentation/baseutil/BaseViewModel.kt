package com.movieapp.presentation.baseutil

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    /**
     * this will keep all disposable together
     */
    private val compositeDisposable = CompositeDisposable()

    /**
     * in this method, we are collecting all disposables, which we will dispose at the time of onCleared()
     */
    protected fun Disposable.add() = compositeDisposable.add(this)

    override fun onCleared() {
        super.onCleared()
        // here, we will dispose all disposable which we have added at the time of destroying state of view model.
        compositeDisposable.dispose()
    }
}