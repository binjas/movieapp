package com.movieapp.presentation.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.movieapp.R
import com.movieapp.domain.history.entity.History
import com.movieapp.presentation.history.listeners.RecyclerViewClickListener
import kotlinx.android.synthetic.main.row_history.view.*

class HistoryAdapter(private val recyclerViewClickListener: RecyclerViewClickListener) :
    RecyclerSwipeAdapter<HistoryAdapter.HistoryViewHolder>() {
    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipeLayout
    }

    private var listHistory = mutableListOf<History>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        return HistoryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_history, parent, false)
        )
    }

    fun removeItem(history: History) {
        listHistory.remove(history)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listHistory.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val current = listHistory[position]
        holder.bind(current)
    }

    fun setData(listHistory: MutableList<History>) {
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    inner class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(current: History) {
            itemView.tvHistory.text = current.text
            itemView.btnDelete.setOnClickListener {
                mItemManger.closeItem(adapterPosition)
                recyclerViewClickListener.onDelete(current)
            }
            itemView.tvHistory.setOnClickListener {
                recyclerViewClickListener.onItemClick(current)
            }
            // need to do this to automatically close the open row items
            mItemManger.bindView(itemView, adapterPosition)
        }

    }
}