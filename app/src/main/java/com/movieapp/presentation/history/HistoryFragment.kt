package com.movieapp.presentation.history

import android.content.Context
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movieapp.R
import com.movieapp.domain.history.entity.History
import com.movieapp.presentation.baseutil.BaseViewModelFragment
import com.movieapp.presentation.history.listeners.RecyclerViewClickListener
import com.movieapp.utils.KeyboardUtils
import com.movieapp.utils.SafeObserver
import com.movieapp.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_history.*

/**
 * Movie Search screen
 */
class HistoryFragment : BaseViewModelFragment<HistoryViewModel>(), RecyclerViewClickListener {

    private val adapterHistory by lazy { HistoryAdapter(this) }
    private lateinit var movieListItemDecoration: DividerItemDecoration

    override fun getInflatingLayout(): Int {
        return R.layout.fragment_history
    }

    override fun buildViewModel(): HistoryViewModel {
        activity?.let {
            val factory = HistoryViewModelFactory()
            return ViewModelProviders.of(it, factory)[HistoryViewModel::class.java]
        }
    }

    override fun initLiveDataObservers() {
        super.initLiveDataObservers()
        viewModel.historyLiveEvent.observe(this, SafeObserver {
            adapterHistory.setData(it)
        })
    }

    override fun initializeViews() {
        //forcefully show the keyboard for searching
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT)

        etSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val keyword = etSearch.text.toString().trim()
                //check validation
                if (viewModel.isInputValid(keyword)) {
                    navigate(keyword)
                    viewModel.insertHistory(keyword)
                    KeyboardUtils.hideKeyboard(activity, etSearch)
                    etSearch.text = null
                } else {
                    ToastUtils.toast(v.context, R.string.please_enter_valid_text)
                }
                return@OnEditorActionListener true
            }
            false
        })

        btClose.setOnClickListener {
            activity?.onBackPressed()
        }

        // setting adapter for showing history
        rvHistory.adapter = adapterHistory
        rvHistory.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        movieListItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        rvHistory.addItemDecoration(movieListItemDecoration)
        viewModel.fetchHistory()
    }

    override fun onItemClick(history: History) {
        // hide keyboard after selecting from history
        KeyboardUtils.hideKeyboard(activity, etSearch)
        navigate(history.text)
    }

    override fun onDelete(history: History) {
        viewModel.deleteItem(history)
        adapterHistory.removeItem(history)
    }

    private fun navigate(keyword: String) {
        val directions = HistoryFragmentDirections.ActionHistoryFragmentToMovieListStatusFragment()
        directions.keyword = keyword
        Navigation.findNavController(etSearch).navigate(directions)
    }
}
