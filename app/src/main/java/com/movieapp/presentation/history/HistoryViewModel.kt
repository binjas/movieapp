package com.movieapp.presentation.history

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.movieapp.domain.history.entity.History
import com.movieapp.domain.movie.usecase.HistoryUseCase
import com.movieapp.extension.isRequiredField
import com.movieapp.presentation.baseutil.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HistoryViewModel(
    private val historyUseCase: HistoryUseCase
) : BaseViewModel() {
    var historyLiveEvent = MutableLiveData<MutableList<History>>()

    @SuppressLint("CheckResult")
    fun fetchHistory() {
        historyUseCase.fetchHistory().subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe { listCategory ->
                historyLiveEvent.value = listCategory
            }
    }

    fun insertHistory(keyword: String) {
        historyUseCase.insertHistory(keyword)
    }

    fun isInputValid(keyword: String): Boolean {
        return keyword.isRequiredField()
    }

    fun deleteItem(history: History) {
        historyUseCase.deleteItem(history)
    }
}