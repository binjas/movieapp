package com.movieapp.presentation.history

import androidx.lifecycle.ViewModel
import com.movieapp.domain.movie.usecase.HistoryUseCase
import com.movieapp.presentation.baseutil.BaseViewModelFactory
import javax.inject.Inject

class HistoryViewModelFactory : BaseViewModelFactory() {
    @Inject
    lateinit var historyUseCase: HistoryUseCase

    init {
        component.inject(this)
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.cast(HistoryViewModel(historyUseCase)) as T
    }
}
