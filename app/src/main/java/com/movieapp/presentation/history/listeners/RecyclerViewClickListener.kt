package com.movieapp.presentation.history.listeners

import com.movieapp.domain.history.entity.History

interface RecyclerViewClickListener {
    fun onDelete(history: History)

    fun onItemClick(history: History)
}