package com.movieapp.presentation.movie.home

import android.content.Context
import androidx.lifecycle.ViewModel
import com.movieapp.domain.usecase.HomeUseCase
import com.movieapp.presentation.baseutil.BaseViewModelFactory
import javax.inject.Inject

class HomeViewModelFactory  : BaseViewModelFactory() {
    @Inject
    lateinit var loginUseCase: HomeUseCase

    @Inject
    lateinit var context: Context

    init {
        component.inject(this)
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.cast(HomeViewModel(context, loginUseCase)) as T
    }
}