package com.movieapp.presentation.movie.home

import android.os.Handler
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.movieapp.R
import com.movieapp.data.common.navigateToFragment
import com.movieapp.data.movie.entity.HomeResponse
import com.movieapp.data.movie.entity.MovieData
import com.movieapp.presentation.baseutil.BaseViewModelFragment
import com.movieapp.utils.KeyUtil.ALPHA_ANIMATION_TIME
import com.movieapp.utils.KeyUtil.AUTO_ANIMATE_TIME
import com.movieapp.utils.KeyUtil.FADE_FACTOR
import com.movieapp.utils.ProgressDialogUtil
import com.movieapp.utils.SafeObserver
import com.movieapp.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_movie.*

class HomeFragment : BaseViewModelFragment<HomeViewModel>(), View.OnClickListener {
    private var movieDataList: MutableList<MovieData> = ArrayList()
    // current page variable
    private var currentPage = 0
    // Handler for the auto scroll
    private var handler = Handler()
    private var runnable = Runnable {
        // set viewpager's position while auto scrolling.
        vpMovie?.setCurrentItem(currentPage, true)
        if (currentPage == movieDataList.size - 1) {
            currentPage = 0
        } else {
            ++currentPage
        }
        pauseAutoScroll()
        setupAutoPager()
    }

    override fun getInflatingLayout(): Int {
        return R.layout.fragment_movie
    }

    override fun initializeViews() {
        ivSearch.setOnClickListener(this)
        if (context != null) {
            if (movieDataList.isEmpty()) {
                // we are showing progress dialog as we need to call api.
                ProgressDialogUtil.showProgressDialog(context!!)
            } else {
                // as we have already data we need to just set in data.
                setMovieList()
            }
        }
    }

    override fun buildViewModel(): HomeViewModel {
        activity?.let {
            val factory = HomeViewModelFactory()
            return ViewModelProviders.of(it, factory)[HomeViewModel::class.java]
        }
    }

    override fun initLiveDataObservers() {
        super.initLiveDataObservers()
        // setting observer to handle success scenario.
        viewModel.homeResponseLiveData.observe(this, safeObserver)
        // setting observer to handle failure scenario.
        viewModel.errorLiveEvent.observe(this, SafeObserver {
            // we are hiding progress dialog as api calling finished.
            // here we can handle Different types of exceptions, likewise No internet connection,
            // for that we can handle that one according to requirement.
            ProgressDialogUtil.hideProgressDialog()
            ToastUtils.toast(context, it.throwable?.message)
        })
    }

    override fun handleBusinessLogic() {
        // checking if context before calling home movie list api.
        if (context != null && movieDataList.isEmpty()) {
            viewModel.callHomeApi()
        }
    }

    private val safeObserver = SafeObserver<HomeResponse?> { it ->
        it?.let { homeResponse ->
            ProgressDialogUtil.hideProgressDialog()
            if (!homeResponse.movieList.isNullOrEmpty()) {
                if (movieDataList.isNotEmpty()) {
                    // Clear list if it's not empty
                    movieDataList.clear()
                }
                // Add list from response
                movieDataList.addAll(homeResponse.movieList!!)
                // set data in viewpager
                setMovieList()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivSearch -> {
                navigateToFragment(R.id.historyFragment)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // start auto scrolling
        setupAutoPager()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // remove callback
        pauseAutoScroll()
    }

    override fun onPause() {
        super.onPause()
        // remove callback
        pauseAutoScroll()
    }

    /**
     * Set viewpager's content
     */
    private fun setMovieList() {
        if (movieDataList.isNullOrEmpty()) return
        // Set adapter
        vpMovie.adapter = MoviePagerAdapter(childFragmentManager, movieDataList)
        // Animate viewpager
        vpMovie.setAnimationEnabled(true)
        // Fade visibility for side views
        vpMovie.setFadeEnabled(true)
        // Fade value
        vpMovie.setFadeFactor(FADE_FACTOR)
        // Set first item movie name
        setupTitleAndCategoryNames(vpMovie.currentItem)
        // Set page change listener
        vpMovie.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                when (state) {
                    // set animation for the movie and category name
                    ViewPager.SCROLL_STATE_IDLE -> {
                        tvName.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(1f).start()
                        tvCategories.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(1f).start()
                    }
                    ViewPager.SCROLL_STATE_DRAGGING, ViewPager.SCROLL_STATE_SETTLING -> {
                        tvName.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(0f).start()
                        tvCategories.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(0f).start()
                    }
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                setupTitleAndCategoryNames(position)
            }
        })
        // pause auto scrolling
        pauseAutoScroll()
        // start auto scrolling
        setupAutoPager()
        // set touch listener for the pause and restart for auto scrolling.
        vpMovie.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // pause auto scrolling
                    pauseAutoScroll()
                    return@setOnTouchListener false
                }
                MotionEvent.ACTION_UP -> {
                    // start auto scrolling
                    setupAutoPager()
                    return@setOnTouchListener false
                }
                else -> {
                    return@setOnTouchListener false
                }
            }
        }
    }

    /**
     * Update movie title and category name according to movie
     */
    private fun setupTitleAndCategoryNames(position: Int) {
        // set value of current page selected.
        currentPage = position
        // set movie name
        tvName.text = movieDataList[position].title
        // set categories name comma separated
        tvCategories.text = movieDataList[position].genreList?.joinToString { it.name!! }
        // set alpha animation while page select
        tvName.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(1f).start()
        tvCategories.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(1f).start()
    }

    /**
     * Pause auto scrolling
     */
    private fun pauseAutoScroll() {
        // remove callbacks of handler
        handler.removeCallbacks(runnable)
    }

    /**
     * Start auto scrolling
     */
    private fun setupAutoPager() {
        // setup handler with delay
        handler.postDelayed(runnable, AUTO_ANIMATE_TIME)
    }
}