package com.movieapp.presentation.movie.home

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.movieapp.R
import com.movieapp.data.common.exceptions.MovieAppHttpException
import com.movieapp.data.movie.entity.HomeResponse
import com.movieapp.domain.usecase.HomeUseCase
import com.movieapp.extension.customSubscribe
import com.movieapp.presentation.baseutil.BaseViewModel
import com.movieapp.presentation.common.Resource
import com.movieapp.presentation.common.Status

class HomeViewModel(private val context: Context, private val homeUseCase: HomeUseCase) : BaseViewModel() {
    var homeResponseLiveData = MutableLiveData<HomeResponse>()
    var errorLiveEvent = MutableLiveData<Resource<Throwable>>()

    /**
     * this function handles rest api call for home movie list
     */
    fun callHomeApi() {
        homeUseCase.execute().customSubscribe({ it ->
            if (it.success) {
                homeResponseLiveData.value = it
            } else {
                errorLiveEvent.value = Resource(Status.ERROR, Throwable(it.errorBean.message))
            }
        }, {
            if (it.response.errorId == MovieAppHttpException.STATUS_CODE_NO_INTERNET) {
                errorLiveEvent.value = Resource(Status.NO_INTERNET, Throwable(context.getString(R.string.no_internet)))
            } else {
                errorLiveEvent.value = Resource(Status.ERROR, it)
            }
        })
    }
}