package com.movieapp.presentation.movie.movieList.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.movieapp.presentation.movie.movieList.tab.MovieListFragment
import com.movieapp.utils.KeyUtil

class MovieListViewPagerAdapter(fragmentManager: FragmentManager, private val tabCount: Int
    , private val searchKeyWord: String ) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            KeyUtil.MOVIE_STATUS_NOW_SHOWING -> MovieListFragment.getInstance(
                searchKeyWord,
                KeyUtil.MOVIE_STATUS_NOW_SHOWING
            )
            KeyUtil.MOVIE_STATUS_UPCOMING -> MovieListFragment.getInstance(
                searchKeyWord,
                KeyUtil.MOVIE_STATUS_UPCOMING
            )
            else -> null
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}