package com.movieapp.presentation.movie.movieList.tab

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.movieapp.R
import com.movieapp.data.common.exceptions.MovieAppHttpException
import com.movieapp.data.movie.entity.SearchMoviesResponse
import com.movieapp.domain.usecase.SearchMoviesUseCase
import com.movieapp.extension.customSubscribe
import com.movieapp.presentation.baseutil.BaseViewModel
import com.movieapp.presentation.common.Resource
import com.movieapp.presentation.common.Status
import com.movieapp.utils.KeyUtil

class MovieListViewModel(private val context: Context, private val searchMoviesUseCase: SearchMoviesUseCase) :
    BaseViewModel() {

    var searchMoviesLiveData = MutableLiveData<SearchMoviesResponse>()
    var errorLiveEvent = MutableLiveData<Resource<Throwable>>()

    /**
     * this function handles rest api call for search movies
     */
    fun callSearchMoviesApi(searchKeyWord: String, offset: Int) {
        val hashMap = HashMap<String, Any>()
        hashMap[KeyUtil.KEYWORD] = searchKeyWord
        hashMap[KeyUtil.OFFSET] = offset
        searchMoviesUseCase.setParams(hashMap)
        searchMoviesUseCase.execute().customSubscribe({ it ->
            if (it.success) {
                searchMoviesLiveData.value = it
            } else {
                errorLiveEvent.value = Resource(Status.ERROR, Throwable(it.errorBean.message))
            }
        }, {
            if (it.response.errorId == MovieAppHttpException.STATUS_CODE_NO_INTERNET) {
                errorLiveEvent.value = Resource(Status.NO_INTERNET, Throwable(context.getString(R.string.no_internet)))
            } else {
                errorLiveEvent.value = Resource(Status.ERROR, it)
            }
        })
    }
}