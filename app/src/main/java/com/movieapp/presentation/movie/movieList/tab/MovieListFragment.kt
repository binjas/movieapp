package com.movieapp.presentation.movie.movieList.tab

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movieapp.R
import com.movieapp.data.movie.entity.SearchMoviesResponse
import com.movieapp.presentation.baseutil.BaseViewModelFragment
import com.movieapp.presentation.movie.movieList.adapter.MovieListAdapter
import com.movieapp.utils.KeyUtil
import com.movieapp.utils.KeyUtil.FULL_BIAS
import com.movieapp.utils.KeyUtil.HALF_BIAS
import com.movieapp.utils.SafeObserver
import com.movieapp.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_movie_list.*

class MovieListFragment : BaseViewModelFragment<MovieListViewModel>() {

    private lateinit var movieListLayoutManager: LinearLayoutManager
    private lateinit var movieListItemDecoration: DividerItemDecoration
    private val movieListAdapter by lazy { MovieListAdapter() }
    private var searchKeyWord = ""
    private var movieStatus = -1
    private var pageNumber = 1 // for first page
    private var isLoading = false
    private var isLastPage = false
    private var constraintSet: ConstraintSet? = null // to set vertical bias for progress bar

    companion object {
        fun getInstance(searchKeyWord: String, movieStatus: Int): MovieListFragment {
            val bundle = Bundle()
            bundle.putString(KeyUtil.SEARCH_KEYWORD, searchKeyWord)
            bundle.putInt(KeyUtil.MOVIE_STATUS, movieStatus)
            val movieListFragment = MovieListFragment()
            movieListFragment.arguments = bundle
            return movieListFragment
        }
    }

    override fun buildViewModel(): MovieListViewModel {
        activity?.let {
            val factory = MovieListViewModelFactory()
            return ViewModelProviders.of(it, factory)[MovieListViewModel::class.java]
        }
    }

    override fun getInflatingLayout(): Int {
        return R.layout.fragment_movie_list
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getBundle()
    }

    override fun initializeViews() {
        movieListLayoutManager = LinearLayoutManager(activity)
        rvMovieList.layoutManager = movieListLayoutManager
        movieListItemDecoration = DividerItemDecoration(context, movieListLayoutManager.orientation)
        rvMovieList.addItemDecoration(movieListItemDecoration)
        rvMovieList.adapter = movieListAdapter

        fetchMovies()

        rvMovieList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = movieListLayoutManager.childCount
                val totalItemCount = movieListLayoutManager.itemCount
                val firstVisibleItemPosition = movieListLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        pageNumber += 1
                        fetchMovies()
                    }
                }
            }
        })
    }

    private fun getBundle() {

        arguments?.let {
            if (it.containsKey(KeyUtil.SEARCH_KEYWORD)) {
                searchKeyWord = it.getString(KeyUtil.SEARCH_KEYWORD, "")
            }
            if (it.containsKey(KeyUtil.MOVIE_STATUS)) {
                movieStatus = it.getInt(KeyUtil.MOVIE_STATUS)
            }
        }
    }

    private fun fetchMovies() {
        isLoading = true
        if (constraintSet == null) {
            constraintSet = ConstraintSet()
        }
        with(constraintSet!!) {
            this.clone(clMainView)
            this.setVerticalBias(
                R.id.progressBar,
                if (pageNumber == 1) {
                    HALF_BIAS
                } else {
                    FULL_BIAS
                }
            )
            this.applyTo(clMainView)
        }
        progressBar.visibility = View.VISIBLE
        viewModel.callSearchMoviesApi(searchKeyWord, pageNumber)
    }

    override fun initLiveDataObservers() {
        super.initLiveDataObservers()
        viewModel.searchMoviesLiveData.observe(this, SafeObserver<SearchMoviesResponse?> { it ->
            it?.let {
                progressBar.visibility = View.GONE
                isLoading = false
                it.results?.let { searchResults ->
                    if (movieStatus == KeyUtil.MOVIE_STATUS_NOW_SHOWING) {
                        movieListAdapter.setList(searchResults.showing)
                    } else {
                        movieListAdapter.setList(searchResults.upcoming)
                    }
                }
            }
        })
        viewModel.errorLiveEvent.observe(this, SafeObserver {
            progressBar.visibility = View.GONE
            isLoading = false
            ToastUtils.toast(context, it.throwable?.message)
        })
    }
}