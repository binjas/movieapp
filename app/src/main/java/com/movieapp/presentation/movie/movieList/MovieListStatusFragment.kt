package com.movieapp.presentation.movie.movieList

import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.movieapp.R
import com.movieapp.presentation.baseutil.BaseFragment
import com.movieapp.presentation.movie.movieList.adapter.MovieListViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_movie_list_status.*

class MovieListStatusFragment : BaseFragment() {

    override fun getInflatingLayout(): Int {
        return R.layout.fragment_movie_list_status
    }

    override fun initializeViews() {
        setHeader()
        val args = MovieListStatusFragmentArgs.fromBundle(arguments!!)
        setUpTabLayoutAndViewPager(args.keyword)
    }

    /**
     * this method will set header and enable back navigation
     */
    private fun setHeader() {
        activity?.let {
            with(activity as AppCompatActivity) {
                this.setSupportActionBar(tbHeaderMovieList)
                this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }
        }
    }

    /**
     * this method will set up tab layout and view pager
     */
    private fun setUpTabLayoutAndViewPager(keyword: String) {
        tlMovieListStatus.addTab(tlMovieListStatus.newTab().setText(resources.getString(R.string.now_showing)))
        tlMovieListStatus.addTab(tlMovieListStatus.newTab().setText(resources.getString(R.string.coming_soon)))
        val viewPagerAdapter =
            MovieListViewPagerAdapter(childFragmentManager, tlMovieListStatus.tabCount, keyword)
        vpMovieListStatus.adapter = viewPagerAdapter
        vpMovieListStatus.offscreenPageLimit = tlMovieListStatus.tabCount
        tlMovieListStatus.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpMovieListStatus.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
        vpMovieListStatus.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(position: Int) {}

            override fun onPageScrolled(position: Int, p1: Float, p2: Int) {}

            override fun onPageSelected(position: Int) {
                tlMovieListStatus.getTabAt(position)!!.select()
            }
        })
    }
}