package com.movieapp.presentation.movie.movieList.tab

import android.content.Context
import androidx.lifecycle.ViewModel
import com.movieapp.domain.usecase.SearchMoviesUseCase
import com.movieapp.presentation.baseutil.BaseViewModelFactory
import javax.inject.Inject

class MovieListViewModelFactory : BaseViewModelFactory() {

    @Inject
    lateinit var context: Context
    @Inject
    lateinit var searchMoviesUseCase: SearchMoviesUseCase

    init {
        component.inject(this)
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.cast(MovieListViewModel(context, searchMoviesUseCase)) as T
    }
}