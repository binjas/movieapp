package com.movieapp.presentation.movie.home

import android.os.Bundle
import android.view.View
import com.movieapp.R
import com.movieapp.data.movie.entity.MovieData
import com.movieapp.presentation.baseutil.BaseFragment
import com.movieapp.extension.getRoundCornerOption
import com.movieapp.extension.loadImage
import com.movieapp.utils.KeyUtil.ALPHA_ANIMATION_TIME
import com.movieapp.utils.KeyUtil.MOVIE_OBJECT
import kotlinx.android.synthetic.main.fragment_movie_child.*

class MovieChildFragment: BaseFragment() {
    // movie data object
    private var movieData: MovieData? = null

    companion object {
        fun getInstance(movieData: MovieData): MovieChildFragment {
            val movieChildFragment = MovieChildFragment()
            val bundle = Bundle()
            bundle.putParcelable(MOVIE_OBJECT, movieData)
            movieChildFragment.arguments = bundle
            return movieChildFragment
        }
    }

    override fun getInflatingLayout(): Int {
        return R.layout.fragment_movie_child
    }

    /**
     * Get data from bundle
     */
    private fun getBundle() {
        val bundle = arguments
        if (bundle != null) {
            if (bundle.containsKey(MOVIE_OBJECT)) {
                movieData = bundle.getParcelable(MOVIE_OBJECT)!!
            }
        }
    }

    override fun handleBusinessLogic() {
        getBundle()
        // Load an image using glide
        ivMovie.loadImage(movieData?.posterPath, ivMovie.getRoundCornerOption())

        // set pre sale tag
        if (movieData?.presaleFlag == 0) {
            tvPreSale.visibility = View.GONE
        } else {
            tvPreSale.visibility = View.VISIBLE
            // animate sale tag while showing
            tvPreSale.animate().setDuration(ALPHA_ANIMATION_TIME).alpha(1f).start()
        }
    }

}