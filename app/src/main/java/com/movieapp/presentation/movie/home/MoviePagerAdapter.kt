package com.movieapp.presentation.movie.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.movieapp.data.movie.entity.MovieData

class MoviePagerAdapter(fragmentManager: FragmentManager, private var moviesList: List<MovieData>) :
    FragmentStatePagerAdapter(fragmentManager) {

    override fun getCount(): Int {
        return moviesList.size
    }

    override fun getItem(position: Int): Fragment {
        return MovieChildFragment.getInstance(moviesList[position])
    }

}