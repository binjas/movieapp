package com.movieapp.presentation.movie.movieList.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.movieapp.R
import com.movieapp.data.movie.entity.MovieData
import com.movieapp.extension.getRoundCornerOption
import com.movieapp.extension.loadImage
import com.movieapp.utils.CalendarUtil
import kotlinx.android.synthetic.main.row_movie_list.view.*

class MovieListAdapter : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private val movies = ArrayList<MovieData>()

    fun setList(stalls: ArrayList<MovieData>) {
        this.movies.addAll(stalls)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_movie_list, parent, false))
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(movies[position]) {
            holder.itemView.ivPoster.loadImage(this.posterPath, holder.itemView.ivPoster.getRoundCornerOption())
            holder.itemView.tvName.text = this.title
            holder.itemView.rbRatings.rating = this.rate.toFloat()
            holder.itemView.tvAverageRatings.text = this.rate.toString()
            holder.itemView.tvAgeCategory.text = this.ageCategory
            holder.itemView.tvReleaseDate.text = CalendarUtil.getDateFromTimestamp(this.releaseDate)
            holder.itemView.tvDescription.text = this.description
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}