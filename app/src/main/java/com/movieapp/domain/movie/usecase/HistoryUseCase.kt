package com.movieapp.domain.movie.usecase

import com.movieapp.domain.common.base.BaseUseCase
import com.movieapp.domain.history.HistoryRepository
import com.movieapp.domain.history.entity.History
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HistoryUseCase @Inject constructor(
    private val historyRepository: HistoryRepository
) : BaseUseCase<MutableList<History>>() {
    override fun buildSingle(data: Map<String, Any?>): Single<MutableList<History>> {
        return historyRepository.getHistory()
    }

    fun fetchHistory(): Single<MutableList<History>> {
        return historyRepository.getHistory()
    }

    fun insertHistory(keyword: String) {
        val history = History()
        history.timestamp = System.currentTimeMillis()
        history.text = keyword
        Single.fromCallable {
            historyRepository.insertReplaceHistory(history)
            historyRepository.deleteOldHistory()
        }.subscribeOn(Schedulers.newThread()).subscribe()
    }

    fun deleteItem(history: History) {
        Single.fromCallable {
            historyRepository.delete(history)
        }.subscribeOn(Schedulers.newThread()).subscribe()
    }

}
