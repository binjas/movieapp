package com.movieapp.domain.movie.repository

import com.movieapp.data.movie.entity.HomeResponse
import com.movieapp.data.movie.entity.SearchMoviesResponse
import io.reactivex.Single

interface MovieRepository {

    fun homeRequest(): Single<HomeResponse>

    fun searchRequest(hashMap: HashMap<String, Any>): Single<SearchMoviesResponse>
}
