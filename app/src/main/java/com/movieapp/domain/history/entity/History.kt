package com.movieapp.domain.history.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = History.TABLE_NAME)
data class History(@ColumnInfo(name = COLUMN_ID) @PrimaryKey(autoGenerate = true) val id: Long? = null) {

    @ColumnInfo(name = COLUMN_TEXT)
    lateinit var text: String

    @ColumnInfo(name = COLUMN_TIME)
    var timestamp: Long = 0

    companion object {
        const val TABLE_NAME = "History"
        const val COLUMN_ID = "id"
        const val COLUMN_TEXT = "text"
        const val COLUMN_TIME = "timestamp"
    }
}