package com.movieapp.domain.history

import com.movieapp.domain.history.entity.History
import io.reactivex.Single

interface HistoryRepository {
    fun getHistory(): Single<MutableList<History>>

    fun insertReplaceHistory(history: History): Long

    fun deleteOldHistory()

    fun delete(history: History)
}