package com.movieapp.domain.common

import com.movieapp.data.common.entity.ErrorResponse
import com.movieapp.data.common.exceptions.MovieAppHttpException
import io.reactivex.functions.Consumer
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.net.UnknownServiceException

abstract class CustomErrorConsumer : Consumer<Throwable> {

    override fun accept(throwable: Throwable) {
        if (throwable is MovieAppHttpException) {
            accept(throwable)
        } else {
            val errorBody = ErrorResponse()
            errorBody.message = throwable.message
            errorBody.message = throwable.message
            accept(createLionParcelException(throwable, errorBody))
        }
    }

    private fun createLionParcelException(throwable: Throwable, errorBody: ErrorResponse): MovieAppHttpException {
        return if (throwable is UnknownHostException ||
            throwable is ConnectException ||
            throwable is SocketTimeoutException ||
            throwable is UnknownServiceException
        ) {
            MovieAppHttpException.MovieMovieAppNetWorkHttpException(errorBody)
        } else {
            MovieAppHttpException(errorBody)
        }
    }

    abstract fun accept(it: MovieAppHttpException)
}