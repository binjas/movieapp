package com.movieapp.domain.usecase

import com.movieapp.data.movie.entity.SearchMoviesResponse
import com.movieapp.domain.common.base.BaseUseCase
import com.movieapp.domain.movie.repository.MovieRepository
import io.reactivex.Single
import javax.inject.Inject

class SearchMoviesUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) : BaseUseCase<SearchMoviesResponse>() {

    private var hashMap = HashMap<String, Any>()

    fun setParams(hashMap: HashMap<String, Any>) {
        this.hashMap = hashMap
    }

    override fun buildSingle(data: Map<String, Any?>): Single<SearchMoviesResponse> {
        return movieRepository.searchRequest(hashMap)
    }
}