package com.movieapp.domain.usecase

import com.movieapp.data.movie.entity.HomeResponse
import com.movieapp.domain.common.base.BaseUseCase
import com.movieapp.domain.movie.repository.MovieRepository
import io.reactivex.Single
import javax.inject.Inject

class HomeUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) : BaseUseCase<HomeResponse>() {
    override fun buildSingle(data: Map<String, Any?>): Single<HomeResponse> {
        return movieRepository.homeRequest()
    }
}