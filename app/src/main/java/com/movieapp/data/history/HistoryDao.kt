package com.movieapp.data.history

import androidx.room.Dao
import androidx.room.Query
import com.movieapp.data.common.base.BaseDao
import com.movieapp.domain.history.entity.History
import io.reactivex.Single

@Dao
interface HistoryDao : BaseDao<History> {

    @Query("SELECT * FROM ${History.TABLE_NAME} ORDER BY ${History.COLUMN_TIME} DESC")
    fun getItems(): Single<MutableList<History>>

    @Query("DELETE FROM ${History.TABLE_NAME}")
    fun deleteAll()

    @Query("DELETE FROM ${History.TABLE_NAME} where ${History.COLUMN_ID} NOT IN (SELECT ${History.COLUMN_ID} from ${History.TABLE_NAME} ORDER BY ${History.COLUMN_ID} DESC LIMIT 10)")
    fun deleteOldHistory()
}