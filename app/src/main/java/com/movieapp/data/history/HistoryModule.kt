package com.movieapp.data.history

import com.movieapp.data.RoomDatabase
import com.movieapp.data.common.module.DatabaseModule
import com.movieapp.domain.history.HistoryRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [DatabaseModule::class])
class HistoryModule {

    @Singleton
    @Provides
    fun provideDao(database: RoomDatabase): HistoryDao {
        return database.historyDao()
    }

    @Singleton
    @Provides
    fun provideRepository(dao: HistoryDao): HistoryRepository {
        return HistoryDataRepository(dao)
    }
}
