package com.movieapp.data.history

import com.movieapp.domain.history.HistoryRepository
import com.movieapp.domain.history.entity.History
import io.reactivex.Single
import javax.inject.Inject

class HistoryDataRepository @Inject constructor(
    private val dao: HistoryDao
) : HistoryRepository {
    override fun delete(history: History) {
        return dao.delete(history)
    }

    override fun deleteOldHistory() {
        return dao.deleteOldHistory()
    }

    override fun getHistory(): Single<MutableList<History>> {
        return dao.getItems()
    }

    override fun insertReplaceHistory(history: History): Long {
        return dao.insert(history)
    }

}
