package com.movieapp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.movieapp.BuildConfig
import com.movieapp.data.history.HistoryDao
import com.movieapp.domain.history.entity.History

@Database(
    entities = [History::class],
    version = BuildConfig.VERSION_CODE
)
abstract class RoomDatabase : RoomDatabase() {
    abstract fun historyDao(): HistoryDao
}
