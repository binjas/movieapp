package com.movieapp.data.movie

import com.movieapp.data.movie.entity.HomeResponse
import com.movieapp.data.movie.entity.SearchMoviesResponse
import com.movieapp.domain.movie.repository.MovieRepository
import io.reactivex.Single
import javax.inject.Inject

class MovieDataRepository @Inject constructor(
    private val api: MovieApi
) : MovieRepository {

    override fun homeRequest(): Single<HomeResponse> {
        return api.homeMovieList()
    }

    override fun searchRequest(hashMap: HashMap<String, Any>): Single<SearchMoviesResponse> {
        return api.searchMovies(hashMap)
    }
}
