package com.movieapp.data.movie.entity

import com.movieapp.data.common.entity.BaseResponse

class SearchMoviesResponse : BaseResponse() {
    var results: SearchResults ? = null
}