package com.movieapp.data.movie.entity

class GenreData {
    var id: String? = null
    var name: String? = null
}