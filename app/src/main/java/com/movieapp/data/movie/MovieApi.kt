package com.movieapp.data.movie

import com.movieapp.data.movie.entity.HomeResponse
import com.movieapp.data.movie.entity.SearchMoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MovieApi {

    @GET("home")
    fun homeMovieList(): Single<HomeResponse>

    @GET("search")
    fun searchMovies(@QueryMap queryMap: HashMap<String, Any>): Single<SearchMoviesResponse>
}