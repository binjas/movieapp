package com.movieapp.data.movie.entity

class SearchResults {
    lateinit var upcoming: ArrayList<MovieData>
    lateinit var showing: ArrayList<MovieData>
}