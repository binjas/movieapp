package com.movieapp.data.movie

import android.content.Context
import com.movieapp.data.common.module.DatabaseModule
import com.movieapp.data.common.module.NetworkModule
import com.movieapp.domain.movie.repository.MovieRepository
import com.movieapp.presentation.app.MovieApp
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [NetworkModule::class, DatabaseModule::class])
class MovieModule {

    @Singleton
    @Provides
    fun provideApi(retrofit: Retrofit): MovieApi {
        return retrofit.create(MovieApi::class.java)
    }

    @Singleton
    @Provides
    fun provideContext(): Context {
        return MovieApp.instance
    }

    @Singleton
    @Provides
    fun provideRepository(api: MovieApi): MovieRepository {
        return MovieDataRepository(api)
    }
}
