package com.movieapp.data.movie.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.movieapp.data.common.entity.BaseResponse

class HomeResponse : BaseResponse() {
    @Expose
    @SerializedName("results")
    var movieList: MutableList<MovieData> ? = null
}