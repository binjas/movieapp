package com.movieapp.data.movie.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MovieData() : Parcelable {

    var id: String? = null

    var title: String? = null

    @Expose
    @SerializedName("genre_ids")
    var genreList: MutableList<GenreData>? = null

    @SerializedName("age_category")
    var ageCategory: String? = null

    var rate: Double = 0.0

    @SerializedName("release_date")
    var releaseDate: Long = 0L

    @SerializedName("poster_path")
    var posterPath: String? = null

    @SerializedName("presale_flag")
    var presaleFlag: Int = 0

    var description: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        title = parcel.readString()
        ageCategory = parcel.readString()
        rate = parcel.readDouble()
        releaseDate = parcel.readLong()
        posterPath = parcel.readString()
        presaleFlag = parcel.readInt()
        description = parcel.readString()
    }

    companion object CREATOR : Parcelable.Creator<MovieData> {
        override fun createFromParcel(parcel: Parcel): MovieData {
            return MovieData(parcel)
        }

        override fun newArray(size: Int): Array<MovieData?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(ageCategory)
        parcel.writeDouble(rate)
        parcel.writeLong(releaseDate)
        parcel.writeString(posterPath)
        parcel.writeInt(presaleFlag)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }
}