package com.movieapp.data.common

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.movieapp.R


/**
 * this method is used to handle navigating between different fragments.
 */
fun Fragment.navigateToFragment(
    destinationFragment: Int,
    navOption: NavOptions.Builder? = noAnim(),
    popFragmentId: Int = this.id,
    navHostId: Int = R.id.navGraphMainContainer,
    mActivity: AppCompatActivity = this.activity as AppCompatActivity
) {
    Navigation.findNavController(mActivity, navHostId).navigate(
        destinationFragment,
        null,
        navOption?.setPopUpTo(
            popFragmentId,
            false
        )?.build()
    )
}

fun noAnim(): NavOptions.Builder {
    return NavOptions.Builder()
        .setLaunchSingleTop(true)
}