package com.movieapp.data.common.exceptions

import com.movieapp.data.common.entity.ErrorResponse

open class MovieAppHttpException(val response: ErrorResponse) : Exception() {
    // below are the common http exception we may get during api calling.
    companion object {
        const val STATUS_CODE_NO_INTERNET = 0
        const val STATUS_CODE_NO_CONTENT = 204
        const val STATUS_CODE_BAD_REQUEST = 400
        const val STATUS_CODE_UNAUTHORIZED = 401
        const val STATUS_CODE_FORBIDDEN = 403
        const val STATUS_CODE_UNPROCESSABLE = 422
        const val STATUS_CODE_SERVER_ERROR = 500
    }

    class MovieMovieAppBadRequestHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppForbiddenHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppNetWorkHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppServerHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppUnauthorizedHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppUnprocessableHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppNoInternetHttpException(response: ErrorResponse) : MovieAppHttpException(response)
    class MovieMovieAppNoContentHttpException(response: ErrorResponse) : MovieAppHttpException(response)
}