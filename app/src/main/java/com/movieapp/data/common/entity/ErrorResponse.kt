package com.movieapp.data.common.entity

import com.google.gson.annotations.SerializedName

class ErrorResponse {

    var errorId: Int = 0

    @SerializedName("message")
    var message: String? = null
}