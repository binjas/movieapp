package com.movieapp.data.common

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.movieapp.BuildConfig
import com.movieapp.R
import com.movieapp.data.common.entity.ErrorResponse
import com.movieapp.data.common.exceptions.MovieAppHttpException
import com.movieapp.utils.NetworkUtil.isNetworkAvailable
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody

class ErrorInterceptor(val context: Context) : Interceptor {

    private var gson = Gson()

    override fun intercept(chain: Interceptor.Chain?): Response {
        if (!isNetworkAvailable(context)) {
            val errorBody = ErrorResponse()
            errorBody.errorId = MovieAppHttpException.STATUS_CODE_NO_INTERNET
            errorBody.message = context.getString(R.string.no_internet)
            throw createLionParcelException(MovieAppHttpException.STATUS_CODE_NO_INTERNET, errorBody)
        }
        val request = chain?.request()
        val response = request?.let { chain.proceed(it) } ?: throw MovieAppHttpException(ErrorResponse())

        return if (response.isSuccessful) {
            if (response.code() == 204) {
                val errorBody = ErrorResponse()
                errorBody.errorId = response.code()
                errorBody.message = response.message()
                throw createLionParcelException(response.code(), errorBody)
            } else {
                response
            }
        } else {
            val body = response.body()?.string()?.let {
                var errorBody: ErrorResponse
                try {
                    errorBody = gson.fromJson(it, ErrorResponse::class.java)
                } catch (exception: JsonSyntaxException) {
                    errorBody = ErrorResponse()
                    errorBody.errorId = response.code()
                    errorBody.message = response.message()
                }
                if (BuildConfig.DEBUG) {
                    Log.d("OkHttp", gson.toJson(errorBody))
                }
                throw createLionParcelException(response.code(), errorBody)
            } ?: ""
            response.newBuilder()
                .body(ResponseBody.create(MediaType.parse("application/json"), body))
                .build()
        }
    }

    private fun createLionParcelException(code: Int, errorBody: ErrorResponse): MovieAppHttpException {
        return when (code) {
            MovieAppHttpException.STATUS_CODE_BAD_REQUEST -> MovieAppHttpException.MovieMovieAppBadRequestHttpException(
                errorBody
            )
            MovieAppHttpException.STATUS_CODE_NO_CONTENT -> MovieAppHttpException.MovieMovieAppNoContentHttpException(
                errorBody
            )
            MovieAppHttpException.STATUS_CODE_UNAUTHORIZED -> MovieAppHttpException.MovieMovieAppUnauthorizedHttpException(
                errorBody
            )
            MovieAppHttpException.STATUS_CODE_FORBIDDEN -> MovieAppHttpException.MovieMovieAppForbiddenHttpException(
                errorBody
            )
            MovieAppHttpException.STATUS_CODE_UNPROCESSABLE -> MovieAppHttpException.MovieMovieAppUnprocessableHttpException(
                errorBody
            )
            MovieAppHttpException.STATUS_CODE_NO_INTERNET -> MovieAppHttpException.MovieMovieAppNoInternetHttpException(
                errorBody
            )
            MovieAppHttpException.STATUS_CODE_SERVER_ERROR -> MovieAppHttpException.MovieMovieAppServerHttpException(
                errorBody
            )
            else -> MovieAppHttpException(errorBody)
        }
    }
}