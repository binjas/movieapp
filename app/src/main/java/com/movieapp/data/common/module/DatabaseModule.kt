package com.movieapp.data.common.module

import androidx.room.Room
import androidx.room.RoomDatabase.JournalMode
import com.movieapp.BuildConfig
import com.movieapp.data.RoomDatabase
import com.movieapp.presentation.app.MovieApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideRoomDatabase(): RoomDatabase {
        return Room.databaseBuilder(MovieApp.instance, RoomDatabase::class.java, BuildConfig.DATABASE_NAME)
            .setJournalMode(JournalMode.TRUNCATE)
            .build()
    }
}