package com.movieapp.data.common.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse {
    var success = false

    @Expose
    @SerializedName("error")
    lateinit var errorBean: ErrorResponse
}